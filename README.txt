INTRODUCTION
------------
This module allows administrators to enable their users to register U2F devices
with their Drupal accounts. After doing so, the user will then be required to
verify the registered device before logging in.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/tfa_u2f

  * To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/project/issues/tfa_u2f


REQUIREMENTS
------------

This module requires the following modules:

  * SSL (https) enabled hosting.
  * Two-Factor Authentication module (https://www.drupal.org/project/tfa)
  * yubico/u2flib-server library (https://packagist.org/packages/yubico/u2flib-server)


INSTALLATION
------------

Since this module requires an additional library, it is recommended you install it
using Composer. If you do not have a composer-based Drupal installation, you will
need to acquire the yubico/u2flib-server library by some other means.


CONFIGURATION
-------------

Configure TFA to use this plugin at Configuration » People » Two-Factor Authentication:

  * Validation Plugin: TFA Universal 2nd Factor

  * (optional) Validation Fallback Plugins: TFA Recovery Code

  * Extra Settings

    Application ID: Your website base URL. Ex: https://example.org

    Challenge Text: Any arbitrary text that will be encoded and used during device
    registration and verification.

    Timeout: Amount of time a user has to insert and verify their device.

Configure your Drupal account to use a U2F device at user/{uid}/security/tfa

  * Click "Setup new device"

  * Provide your account password

  * Insert your device and touch the button on it.

  * Provide a name for this new device, and submit the form.


MAINTAINERS
-----------

Current maintainers:
 * Jonathan Daggerhart (daggerhart) - https://drupal.org/u/daggerhart
