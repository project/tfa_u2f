(function ($, Drupal, drupalSettings, u2f) {

  'use strict';

  Drupal.behaviors.tfa_u2f = {
    attach: function (context, settings) {
      var $status = $('.u2f-device-status');
      var $submitButton = $('.u2f-submit-button');
      var $keyField = $('.u2f-key-registration-target');
      var $labelField = $('.u2f-key-label-field');

      $keyField.closest('.form-item').hide();
      $labelField.closest('.form-item').hide();
      $submitButton.hide();

      if (u2f.register) {
        $status.text(drupalSettings.tfa_u2f.labels.waiting);
      }
      else {
        $status.text(drupalSettings.tfa_u2f.labels.error);
      }

      u2f.register(drupalSettings.tfa_u2f.registerRequests, [], function(data) {
        if (data.registrationData) {
          $keyField.val(JSON.stringify(data));
          $status.text(drupalSettings.tfa_u2f.labels.received);
          $labelField.closest('.form-item').show();
          $submitButton.show();
        }
        else {
          $status.text(drupalSettings.tfa_u2f.labels.error);
        }
      }, drupalSettings.tfa_u2f.timeout);
    }
  };

})(jQuery, Drupal, drupalSettings, u2f);
