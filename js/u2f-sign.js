(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.tfa_u2f = {
    attach: function (context, settings) {
      var $status = $('.u2f-device-status');
      var $submitButton = $('.u2f-submit-button');
      var $signField = $('.u2f-key-sign-target');

      $signField.closest('.form-item').hide();
      $submitButton.hide();

      if (u2f.sign) {
        $status.text(drupalSettings.tfa_u2f.labels.waiting);
      }
      else {
        $status.text(drupalSettings.tfa_u2f.labels.error);
      }

      u2f.sign(drupalSettings.tfa_u2f.signRequests, function(data) {
        if (data.signatureData) {
          $('.u2f-key-sign-target').val(JSON.stringify(data));
          $status.text(drupalSettings.tfa_u2f.labels.received);
          $submitButton.show();
        }
        else {
          $status.text(drupalSettings.tfa_u2f.labels.error);
        }
      }, drupalSettings.tfa_u2f.timeout);
    }
  };

})(jQuery, Drupal, drupalSettings);
