<?php

namespace Drupal\tfa_u2f\Plugin\TfaSetup;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\Plugin\TfaSetupInterface;
use Drupal\tfa_u2f\TfaU2fBasePlugin;
use Drupal\user\UserDataInterface;

/**
 * Class TfaU2fSetup.
 *
 * @TfaSetup(
 *   id = "tfa_u2f_setup",
 *   label = @Translation("TFA U2F Setup"),
 *   description = @Translation("TFA U2F Setup Plugin"),
 *   setupMessages = {
 *    "saved" = @Translation("New device successfully setup."),
 *    "skipped" = @Translation("Device setup skipped.")
 *   },
 *   helpLinks = {
 *    "About FIDO U2F Keys" = "https://www.yubico.com/products/yubikey-hardware/fido-u2f-security-key/",
 *   },
 * )
 */
class TfaU2fSetup extends TfaU2fBasePlugin implements TfaSetupInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, EncryptionProfileManagerInterface $encryption_profile_manager, EncryptServiceInterface $encrypt_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $user_data, $encryption_profile_manager, $encrypt_service);
  }

  /**
   * {@inheritdoc}
   */
  public function getHelpLinks() {
    return ($this->pluginDefinition['helpLinks']) ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function getSetupMessages() {
    return ($this->pluginDefinition['setupMessages']) ?: '';
  }

  /**
   * Determine if the plugin can run for the current TFA context.
   *
   * @return bool
   *    True or False based on the checks performed.
   */
  public function ready() {
    $devices = $this->getUserDevices();
    return !empty($devices);
  }

  /**
   * Plugin overview page.
   *
   * @param array $params
   *   Parameters to setup the overview information.
   *
   * @return array
   *   The overview form.
   */
  public function getOverview($params) {
    $help_links = $this->getHelpLinks();
    $items = [];
    foreach ($help_links as $item => $link) {
      $items[] = Link::fromTextAndUrl($item, Url::fromUri($link, ['attributes' => ['target' => '_blank']]));
    }
    $markup = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => $this->t('Help links:'),
    ];

    $registrations = $this->getUserDeviceRegistrations();
    $devices = [];
    foreach ($registrations as $machine_name => $registration) {
      $reset_url = Url::fromRoute('tfa.plugin.reset', [
        'user' => $params['account']->id(),
        'method' => $params['plugin_id'],
        'reset' => $machine_name,
      ]);
      $devices[] = $this->t('@label - @reset_link', [
        '@label' => $registration->label,
        '@reset_link' => Link::fromTextAndUrl($this->t('remove device'), $reset_url)->toString(),
      ]);
    }

    $admin_links = [
      'admin' => [
        'title' => empty($devices) ? $this->t('Set up new device') : $this->t('Add another device'),
        'url' => Url::fromRoute('tfa.validation.setup', [
          'user' => $params['account']->id(),
          'method' => $params['plugin_id'],
        ]),
      ],
    ];

    if (!empty($devices)) {
      $admin_links['reset'] = [
        'title' => $this->t('Remove all devices'),
        'url' => Url::fromRoute('tfa.plugin.reset', [
          'user' => $params['account']->id(),
          'method' => $params['plugin_id'],
          'reset' => 'removeAll',
        ]),
      ];
    }

    $output = [
      'heading' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => t('TFA Universal 2nd-Factor'),
      ],
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('Generate verification codes from a U2F hardware device.'),
      ],
      'help_links' => [
        '#type' => 'markup',
        '#markup' => \Drupal::service('renderer')->render($markup),
      ],
      'devices' => [
        '#theme' => 'item_list',
        '#items' => $devices,
        '#list_type' => 'ol',
        '#title' => $this->t('Devices Registered'),
        '#access' => !empty($devices),
      ],
      'device_links' => [
        '#theme' => 'links',
        '#links' => $admin_links,
      ],
    ];

    return $output;
  }

  /**
   * Get the setup form for the validation method.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form API array.
   */
  public function getSetupForm(array $form, FormStateInterface $form_state, $reset = 0) {
    if (empty($reset)) {
      $form = $this->newDeviceForm($form, $form_state, $reset);
    }
    elseif ($reset === 'removeAll') {
      $form = $this->removeAllDevicesForm($form, $form_state, $reset);
    }
    else {
      $form = $this->removeDeviceForm($form, $form_state, $reset);
    }

    return $form;
  }

  /**
   * Form to setup a new device from a user's account.
   *
   * @param array $form
   *   Setup Form.
   * @param FormStateInterface $form_state
   *   SetupForm form_state.
   * @param mixed $reset
   *   Reset key passed in by route.
   *
   * @return array
   *   Standard Drupal form.
   */
  public function newDeviceForm(array $form, FormStateInterface $form_state, $reset) {
    $status_labels = $this->deviceStatusLabels();
    $form['#title'] = $this->t('Universal 2nd-Factor Device Setup');
    $form['device_status'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $status_labels['preparing'],
      '#attributes' => [
        'class' => ['u2f-device-status'],
      ],
    ];
    $form['logo'] = [
      '#theme' => 'image',
      '#uri' => file_create_url(drupal_get_path('module', 'tfa_u2f') . '/images/key.png'),
    ];
    $form['code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Device verification: Touch your key to register it.'),
      '#required'  => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
        'class' => ['u2f-key-registration-target'],
      ],
      '#attached' => [
        'drupalSettings' => [
          'tfa_u2f' => [
            'labels' => $status_labels,
            'timeout' => $this->settings['timeout'],
            'registerRequests' => [
              [
                'version' => 'U2F_V2',
                'appId' => $this->settings['application_id'],
                'challenge' => $this->settings['challenge'],
                'sessionId' => \Drupal::currentUser()->id(),
              ],
            ],
          ],
        ],
        'library' => [
          'tfa_u2f/u2f-register',
        ],
      ],
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name this device'),
      '#description' => $this->t('Provide a name that helps you identify the authorized device. Example: Blue Yubikey'),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['u2f-key-label-field'],
      ],
    ];
    $form['machine_name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'source' => ['label'],
        'exists' => [$this, 'userDeviceExists'],
      ],
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#attributes' => [
        'class' => ['u2f-submit-button'],
      ],
    ];

    return $form;
  }

  /**
   * Form to remove a single device from a user's account.
   *
   * @param array $form
   *   Setup Form.
   * @param FormStateInterface $form_state
   *   SetupForm form_state.
   * @param mixed $reset
   *   Reset key passed in by route.
   *
   * @return array
   *   Standard Drupal form.
   */
  public function removeDeviceForm(array $form, FormStateInterface $form_state, $reset) {
    $registrations = $this->getUserDeviceRegistrations();
    $device = $registrations[$reset];

    $form['#title'] = $this->t('Confirm remove @device_label U2F device from your account.', [
      '@device_label' => $device->label,
    ]);
    $form['reset'] = [
      '#type' => 'value',
      '#value' => $reset,
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove device'),
    ];

    return $form;
  }

  /**
   * Form to remove all devices from a user's account.
   *
   * @param array $form
   *   Setup Form.
   * @param FormStateInterface $form_state
   *   SetupForm form_state.
   * @param mixed $reset
   *   Reset key passed in by route.
   *
   * @return array
   *   Standard Drupal form.
   */
  public function removeAllDevicesForm(array $form, FormStateInterface $form_state, $reset) {
    $form['#title'] = $this->t('Remove all Universal 2nd-Factor devices from account');
    $form['reset'] = [
      '#type' => 'value',
      '#value' => $reset,
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove devices'),
    ];

    return $form;
  }

  /**
   * Validate the setup data.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateSetupForm(array $form, FormStateInterface $form_state) {
    $reset = $form_state->getValue('reset');

    if (empty($reset)) {
      $json = $form_state->getValue('code');
      $label = $form_state->getValue('label');
      $data = Json::decode($json);
      return (!empty($json) && !empty($data['registrationData']) && !empty($data['clientData']) && !empty($label));
    }
    elseif ($reset === 'removeAll') {
      return TRUE;
    }
    else {
      return $this->userDeviceExists($reset);
    }
  }

  /**
   * Submit the setup form.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE if no errors occur when saving the data.
   */
  public function submitSetupForm(array $form, FormStateInterface $form_state) {
    $reset = $form_state->getValue('reset');

    if (empty($reset)) {
      $json = $form_state->getValue('code');
      $label = $form_state->getValue('label');
      $machine_name = $form_state->getValue('machine_name');
      $this->createUserDeviceRegistration($json, $label, $machine_name);
    }
    elseif ($reset === 'removeAll') {
      $this->deleteAllUserDevices();
    }
    else {
      $this->deleteUserDevice($reset);
    }

    return TRUE;
  }

}
