<?php

namespace Drupal\tfa_u2f\Plugin\TfaValidation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\Plugin\TfaValidationInterface;
use Drupal\tfa_u2f\TfaU2fBasePlugin;
use Drupal\user\UserDataInterface;

/**
 * Class TfaU2fValidation.
 *
 * @package Drupal\tfa_u2f\Plugin\TfaValidation
 *
 * U2F validation class.
 *
 * @TfaValidation(
 *   id = "tfa_u2f",
 *   label = @Translation("TFA Universal 2nd Factor"),
 *   description = @Translation("TFA U2F Validation Plugin"),
 *   fallbacks = {
 *    "tfa_recovery_code"
 *   },
 *   isFallback = FALSE
 * )
 */
class TfaU2fValidation extends TfaU2fBasePlugin implements TfaValidationInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, EncryptionProfileManagerInterface $encryption_profile_manager, EncryptServiceInterface $encrypt_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $user_data, $encryption_profile_manager, $encrypt_service);
  }

  /**
   * Determine if the plugin can run for the current TFA context.
   *
   * @return bool
   *    True or False based on the checks performed.
   */
  public function ready() {
    return TRUE;
  }

  /**
   * Get validation plugin fallbacks.
   *
   * @return string[]
   *   Returns a list of fallback methods available for the current validation
   */
  public function getFallbacks() {
    return ($this->pluginDefinition['fallbacks']) ?: [];
  }

  /**
   * Is the validation plugin a fallback?
   *
   * If the plugin is a fallback we remove it from the validation
   * plugins list and show it only in the fallbacks list.
   *
   * @return bool
   *   TRUE if plugin is a fallback otherwise FALSE
   */
  public function isFallback() {
    return ($this->pluginDefinition['isFallback']) ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm($config, $state) {
    $settings_form['application_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application ID'),
      '#description' => $this->t('This is the base URL for your website. HTTPS is required! Example: https://example.org'),
      '#default_value' => $this->settings['application_id'],
      '#required' => TRUE,
    ];

    $settings_form['challenge_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Challenge Text'),
      '#description' => $this->t('This is some arbitrary text that will be encoded to define a unique challenge for your site. This cannot be changed so pick something good.'),
      '#default_value' => $this->settings['challenge_text'],
      '#disabled' => !empty($this->settings['challenge_text']),
      '#required' => TRUE,
    ];

    $settings_form['timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Amount of time in seconds a user has to push the button on their device.'),
      '#default_value' => $this->settings['timeout'],
      '#required' => TRUE,
    ];

    return $settings_form;
  }

  /**
   * Get TFA process form from plugin.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form API array.
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $signRequests = $this->getUserSignRequests(TRUE);
    $status_labels = $this->deviceStatusLabels();
    $form['#title'] = $this->t('Universal 2nd-Factor Device Login');
    $form['device_status'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $status_labels['preparing'],
      '#attributes' => [
        'class' => ['u2f-device-status'],
      ],
    ];
    $form['logo'] = [
      '#theme' => 'image',
      '#uri' => file_create_url(drupal_get_path('module', 'tfa_u2f') . '/images/key.png'),
    ];
    $form['code'] = [
      '#type' => 'textarea',
      '#title' => t('Application verification code'),
      '#required'  => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
        'class' => ['u2f-key-sign-target'],
      ],
      '#attached' => [
        'drupalSettings' => [
          'tfa_u2f' => [
            'labels' => $status_labels,
            'timeout' => $this->settings['timeout'],
            'signRequests' => $signRequests,
          ],
        ],
        'library' => [
          'tfa_u2f/u2f-sign',
        ],
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['login'] = [
      '#type'  => 'submit',
      '#value' => t('Verify'),
      '#attributes' => [
        'class' => ['u2f-submit-button'],
      ],
    ];

    return $form;
  }

  /**
   * Validate form.
   *
   * @param array $form
   *   The configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Whether form passes validation or not
   */
  public function validateForm(array $form, FormStateInterface $form_state) {
    $json = $form_state->getValue('code');
    return !empty($json) ? $this->validateSignRequest($json) : FALSE;
  }

}
