<?php

namespace Drupal\tfa_u2f;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\Plugin\TfaBasePlugin;
use Drupal\tfa\TfaDataTrait;
use Drupal\user\UserDataInterface;
use u2flib_server\Error;
use u2flib_server\RegisterRequest;
use u2flib_server\U2F;

/**
 * Class TfaU2fBasePlugin.
 *
 * @package Drupal\tfa_u2f
 */
abstract class TfaU2fBasePlugin extends TfaBasePlugin {

  use TfaDataTrait;
  use StringTranslationTrait;

  /**
   * This plugin's settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * UserData service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Tfa Plugin definition / annotation details.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * Transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, EncryptionProfileManagerInterface $encryption_profile_manager, EncryptServiceInterface $encrypt_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $user_data, $encryption_profile_manager, $encrypt_service);
    $this->userData = $user_data;
    $this->pluginDefinition = $plugin_definition;
    $this->settings = $this->getSettings();
    $this->transliteration = \Drupal::transliteration();
  }

  /**
   * Common labels when dealing with a device at registration or validation.
   *
   * @return array
   *   Array of translatable labels.
   */
  protected function deviceStatusLabels() {
    $labels = [
      'preparing' => $this->t('Preparing to receive device input.'),
      'waiting' => $this->t('Press the button on your device.'),
      'received' => $this->t('Device found.'),
      'error' => $this->t('There was an error with the device or your browser. Please try again.'),
    ];

    return $labels;
  }

  /**
   * Helper function to get and preprocess this module's tfa plugin settings.
   *
   * @return array
   *   Settings array.
   */
  protected function getSettings() {
    $plugin_settings = \Drupal::config('tfa.settings')->get('validation_plugin_settings');
    $stored_settings = !empty($plugin_settings['tfa_u2f']) ? $plugin_settings['tfa_u2f'] : [];
    $settings = array_replace([
      'application_id' => ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'],
      'challenge_text' => '',
      'timeout' => 30,
    ], $stored_settings);

    $settings['challenge'] = base64_encode($settings['challenge_text']);
    $settings['timeout'] = intval($settings['timeout']);

    return $settings;
  }

  /**
   * Register a new device to the current user's data.
   *
   * @param string $device_json
   *   Response from registration as json data.
   * @param string $label
   *   Human readable name.
   */
  protected function createUserDeviceRegistration($device_json, $label, $machine_name) {
    $devices = $this->getUserData('tfa_u2f', 'devices', $this->uid, $this->userData);
    $u2f = new U2F($this->settings['application_id']);
    $request = new RegisterRequest($this->settings['challenge'], $this->settings['application_id']);
    $registration = $u2f->doRegister($request, json_decode($device_json));

    $complete_registration = new TfaU2fRegistration($this->uid, $this->userData, $this->encryptService, $this->encryptionProfile);
    $complete_registration->populateFromSetup($this->settings, $registration, $label, $machine_name);


    $record = ['devices' => $devices];
    $record['devices'][$machine_name] = $complete_registration->toString();

    $this->setUserData('tfa_u2f', $record, $this->uid, $this->userData);
  }

  /**
   * Get a list of all user devices.
   *
   * @return array
   *   Stored device registrations, still serialized and encrypted.
   */
  protected function getUserDevices() {
    $devices = $this->getUserData('tfa_u2f', 'devices', $this->uid, $this->userData);
    return !empty($devices) ? $devices : [];
  }

  /**
   * Determine if a device on the account already has the given machine name.
   *
   * @param string $machine_name
   *   A device machine name.
   *
   * @return bool
   *   Device existence by machine name.
   */
  public function userDeviceExists($machine_name) {
    $devices = $this->getUserDeviceRegistrations();
    return isset($devices[$machine_name]);
  }

  /**
   * Remove a specific device from the user's stored devices.
   *
   * @param string $machine_name
   *   Normalized label for a device.
   */
  protected function deleteUserDevice($machine_name) {
    $devices = $this->getUserDevices();
    unset($devices[$machine_name]);
    $record = ['devices' => $devices];
    $this->setUserData('tfa_u2f', $record, $this->uid, $this->userData);
  }

  /**
   * Remove all of the user's registered devices.
   */
  protected function deleteAllUserDevices() {
    $this->deleteUserData('tfa_u2f', 'devices', $this->uid, $this->userData);
  }

  /**
   * Get list of authorized devices.
   *
   * @return TfaU2fRegistration[]
   *   List of current registered devices.
   */
  protected function getUserDeviceRegistrations() {
    $devices = $this->getUserDevices();
    $registrations = [];

    if (!empty($devices)) {
      foreach ($devices as $machine_name => $device) {
        $registration = new TfaU2fRegistration($this->uid, $this->userData, $this->encryptService, $this->encryptionProfile);
        $registration->populateFromUserData($device);
        $registrations[$machine_name] = $registration;
      }
    }

    return $registrations;
  }

  /**
   * Get an array of sign requests for the user in question.
   *
   * @param bool $asArrays
   *   Determine if the signRequests should be returned as arrays.
   *
   * @return \Drupal\tfa_u2f\TfaU2fSignRequest[]
   *   Array of sign requests.
   */
  protected function getUserSignRequests($asArrays = FALSE) {
    $sign_requests = [];
    $registrations = $this->getUserDeviceRegistrations();

    foreach ($registrations as $registration) {
      $sign_request = new TfaU2fSignRequest($registration);
      $sign_requests[] = $asArrays ? $sign_request->toArray() : $sign_request;
    }

    return $sign_requests;
  }

  /**
   * Validate a device during login.
   *
   * @param string $json
   *   Device registration data as json.
   *
   * @return bool
   *   Result of device validation.
   */
  protected function validateSignRequest($json) {
    $registrations = $this->getUserDeviceRegistrations();
    $signRequests = $this->getUserSignRequests();
    $u2f = new U2F($this->settings['application_id']);
    $response = json_decode($json);

    try {
      $registration = $u2f->doAuthenticate($signRequests, $registrations, $response);
      $this->updateUserDeviceRegistration($registration);
      return TRUE;
    }
    catch (Error $error) {
      return FALSE;
    }
  }

  /**
   * Update a specific device within user's stored devices.
   *
   * @param \Drupal\tfa_u2f\TfaU2fRegistration $registration
   *   Device registration.
   */
  protected function updateUserDeviceRegistration(TfaU2fRegistration $registration) {
    $devices = $this->getUserDevices();
    $devices[$registration->machineName] = $registration->toString();
    $record = ['devices' => $devices];
    $this->setUserData('tfa_u2f', $record, $this->uid, $this->userData);
  }

}
