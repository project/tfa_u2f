<?php

namespace Drupal\tfa_u2f;

use Drupal\encrypt\EncryptionProfileInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\user\UserDataInterface;
use u2flib_server\Registration;

/**
 * Class TfaU2fRegistration.
 *
 * @package Drupal\tfa_u2f
 */
class TfaU2fRegistration extends Registration {

  public $label;

  public $machineName;

  public $appId;

  public $challenge;

  public $keyHandle;

  public $publicKey;

  public $certificate;

  public $counter;

  public $version;

  protected $uid;

  /**
   * Encryption Service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface
   */
  protected $encryptService;

  /**
   * Encryption Profile used by TFA module.
   *
   * @var \Drupal\encrypt\EncryptionProfileInterface
   */
  protected $encryptProfile;

  /**
   * User data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * TfaU2fRegistration constructor.
   *
   * @param int $uid
   *   User Id registering.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User Data service.
   * @param \Drupal\encrypt\EncryptServiceInterface $encrypt_service
   *   Encryption Service.
   * @param \Drupal\encrypt\EncryptionProfileInterface $encryption_profile
   *   Encryption Profile for TFA.
   */
  public function __construct($uid, UserDataInterface $user_data, EncryptServiceInterface $encrypt_service, EncryptionProfileInterface $encryption_profile) {
    $this->uid            = $uid;
    $this->userData       = $user_data;
    $this->encryptService = $encrypt_service;
    $this->encryptProfile = $encryption_profile;
  }

  /**
   * Populate this object during device registration. Aka, Setup.
   *
   * @param array $plugin_settings
   *   Settings for TFA U2F.
   * @param \u2flib_server\Registration $registration
   *   Registration object created buy u2flib_server library.
   * @param string $label
   *   Human name for this device.
   * @param string $machine_name
   *   Machine safe name for this device.
   */
  public function populateFromSetup(array $plugin_settings, Registration $registration, $label, $machine_name) {
    $this->label = $label;
    $this->machineName = $machine_name;
    $this->appId = $plugin_settings['application_id'];
    $this->challenge = $plugin_settings['challenge'];
    $this->keyHandle = $registration->keyHandle;
    $this->publicKey = $registration->publicKey;
    $this->certificate = $registration->certificate;
    $this->counter = $registration->counter;
    $this->version = 'U2F_V2';
  }

  /**
   * Populate this object from the users existing devices.
   */
  public function populateFromUserData($device) {
    $data = unserialize($device);

    $this->label = $data['label'];
    $this->machineName = $data['machineName'];
    $this->appId = $data['appId'];
    $this->challenge = $data['challenge'];
    $this->keyHandle = $this->decrypt($data['keyHandle']);
    $this->publicKey = $this->decrypt($data['publicKey']);
    $this->certificate = $this->decrypt($data['certificate']);
    $this->counter = $data['counter'];
    $this->version = $data['version'];
  }

  /**
   * Simple encryption helper function.
   *
   * @param string $data
   *   Data to be encrypted.
   *
   * @return string
   *   Encrypted data.
   */
  protected function encrypt($data) {
    return $this->encryptService->encrypt($data, $this->encryptProfile);
  }

  /**
   * Simple decryption helper function.
   *
   * @param string $data
   *   Encrypted data.
   *
   * @return string
   *   Decrypted data.
   */
  protected function decrypt($data) {
    return $this->encryptService->decrypt($data, $this->encryptProfile);
  }

  /**
   * Convert this object into a string for storage.
   *
   * @param bool $encrypted
   *   Whether or not to encrypt data during serialization.
   *
   * @return string
   *   Serialized data.
   */
  public function toString($encrypted = TRUE) {
    return serialize([
      'label' => $this->label,
      'machineName' => $this->machineName,
      'appId' => $this->appId,
      'challenge' => $this->challenge,
      'keyHandle' => $encrypted ? $this->encrypt($this->keyHandle) : $this->keyHandle,
      'publicKey' => $encrypted ? $this->encrypt($this->publicKey) : $this->publicKey,
      'certificate' => $encrypted ? $this->encrypt($this->certificate) : $this->challenge,
      'counter' => $this->counter,
      'version' => $this->version,
    ]);
  }

}
