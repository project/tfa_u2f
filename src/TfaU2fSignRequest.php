<?php

namespace Drupal\tfa_u2f;

use u2flib_server\SignRequest;

/**
 * Class TfaU2fSignRequest.
 *
 * @package Drupal\tfa_u2f
 */
class TfaU2fSignRequest extends SignRequest {

  /**
   * Version of U2F used.
   *
   * @var string
   */
  public $version = 'U2F_V2';

  /**
   * Application ID from device registration.
   *
   * @var string
   */
  public $appId;

  /**
   * Key Handle is generated during device registration.
   *
   * @var string
   */
  public $keyHandle;

  /**
   * Challenge used when registering the device.
   *
   * @var string
   */
  public $challenge;

  /**
   * TfaU2fSignRequest constructor.
   *
   * @param \Drupal\tfa_u2f\TfaU2fRegistration $registration
   *   Registration object.
   */
  public function __construct(TfaU2fRegistration $registration) {
    $this->appId = $registration->appId;
    $this->keyHandle = $registration->keyHandle;
    $this->challenge = $registration->challenge;
  }

  /**
   * Convert this object into an array for json related whatnots.
   *
   * @return array
   *   This data, but an array.
   */
  public function toArray() {
    return [
      'version' => $this->version,
      'appId' => $this->appId,
      'keyHandle' => $this->keyHandle,
      'challenge' => $this->challenge,
    ];
  }

}
